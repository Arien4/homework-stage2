package com.epam.stage2.small;

import java.util.*;

public class Main {
    static ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(5, 4, 6, 5, 5, -1, 5, 6, 7, null));
    static HashMap<Integer, Integer> counts = new HashMap<>();

    public static void main(String[] args) {
        for (Integer num : nums) {
            int i = counts.getOrDefault(num, 0);
            counts.put(num, ++i);
        }

        System.out.println(nums);
        System.out.println(counts);


    }
}
