package com.epam.stage2;

import java.util.Objects;

public class Student {
    private String name;
    private int age;
    private String group;

    public Student() {

    }

    public Student(String name, int age, String group) {
        this.name = name;
        this.age = age;
        this.group = group;
    }

    public Student(String name, String group) {
        this.name = name;
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return getAge() == student.getAge() && Objects.equals(getName(), student.getName()) && Objects.equals(getGroup(), student.getGroup());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAge(), getGroup());
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", group='" + group + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Student st1 = new Student();
        System.out.println(st1);
        Student st2 = new Student("Oleh", "45");
        System.out.println(st2);
        Student st3 = new Student("Oleh", 30, "45");
        System.out.println(st3);
        System.out.println(st2.equals(st3));
        st1.setName("Olha");
        st1.setGroup("45");
        st1.setAge(30);
        st3.setName("Olha");
        System.out.println(st1);
        System.out.println(st3);
        System.out.println(st1.equals(st3));
    }
}
