package com.epam.stage2;

import java.util.ArrayList;
import java.util.List;

public class Model implements Comparable<Integer>{
    private final int value = rand();
    private int min;
    private int max = RAND_MAX;
    public static int RAND_MAX = 100;

    private final List<Integer> attempts = new ArrayList<>();

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public boolean inRange(int i) {
        return i > min && i < max;
    }

    private int rand(int min, int max) {
        return (int) Math.ceil(Math.random()*
                (max - min - 1) + min);
    }

    private int rand() {
        return rand(0, RAND_MAX);
    }

    @Override
    public int compareTo(Integer o) {
        int result;
        if (value < o) {
            result = -1;
        } else if (value > o) {
            result = 1;
        } else result = 0;
        return result;
    }

    public List<Integer> getAttempts() {
        return new ArrayList<>(attempts);
    }

    public void addAttempt(int value) {
        attempts.add(value);
    }

}
