package com.epam.stage2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Controller {
    private final Model model;
    private final View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }


    public void run() {

        final Scanner scanner = new Scanner(System.in);

        while (true) {
            view.printGreeting(model.getMin(), model.getMax());
            int input;
            try {
                input = scanner.nextInt();
            } catch (InputMismatchException e) {
                scanner.nextLine();
                view.printRetryMessage();
                continue;
            }
            if (!model.inRange(input)) {
                view.printRetryRangeMessage(model.getMin(), model.getMax());
                continue;
            }
            model.addAttempt(input);
            if (model.compareTo(input) < 0) {
                view.printGreaterMessage();
                model.setMax(input);
            } else if (model.compareTo(input) > 0) {
                view.printLessMessage();
                model.setMin(input);
            } else {
                view.printBingoMessage(model.getAttempts());
                break;
            }
        }
    }


}
