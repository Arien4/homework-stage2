package com.epam.stage2;

import java.util.Arrays;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.IntStream;

public class Main {
    //Задание 1 Stream.
    //
    //Сделать массив int. Из него получить IntStream. Для него
    //
    //1. Найти среднее значение элементов массива
    //
    //2. Найти минимальный элемент, значение и индекс
    //
    //3. Посчитать количество элементов равных нулю
    //
    //4. Посчитать количество элементов больше нуля
    //
    //5. Помножить элементы массива на число
    public static double avg(int[] array) {
        IntStream stream = Arrays.stream(array);
        OptionalDouble res = stream.average();
        return res.orElseThrow(IllegalStateException::new);
    }

    public static int min(int[] array) {
        IntStream stream = Arrays.stream(array);
        OptionalInt res = stream.min();
        return res.orElseThrow(IllegalStateException::new);
    }

    public static int minIndex(int[] array) {
        int min = min(array);
        int res = IntStream.range(0, array.length)
                .filter(i -> min == array[i])
                .findFirst()
                .orElseThrow(IllegalStateException::new);
        return res;
    }

    public static int equalsZero(int[] array) {
        int res = (int) IntStream.range(0, array.length)
                .filter(i -> 0 == array[i])
                .count();
        return res;
    }

    public static int greaterThanZero(int[] array) {
        int res = (int) IntStream.range(0, array.length)
                .filter(i -> array[i] > 0)
                .count();
        return res;
    }

    public static int[] production(int[] array, int number) {
        IntStream stream = Arrays.stream(array).map(a -> a * number);
        return stream.toArray();
    }

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 0, 0, 5, 6, 0, -90};

        //for (int i : array) System.out.println(i);
        // System.out.println(equalsZero(array));
        // System.out.println(greaterThanZero(array));
        int[] prod = production(array, -100);
        for (int i : prod) System.out.println(i);

    }
}
