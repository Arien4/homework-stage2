package com.epam.stage2;

import java.util.Scanner;

public class Record {

    final String REGEX_NAME = "[A-Za-z]{1,15}";
    final String REGEX_NICKNAME = "[A-Za-z0-9_-]{5,15}";

    private View view;
    private Scanner scanner;

    private String firstName;
    private String login;

    public Record(View view, Scanner scanner) {
        this.view = view;
        this.scanner = scanner;
    }

    public void inputRecord() {
        UtilityController uc = new UtilityController(view, scanner);
        this.firstName = uc.inputValue("Enter firstname", REGEX_NAME);
        System.out.println("Firstname entered!");
        this.login = uc.inputValue("Enter nickname", REGEX_NICKNAME);
        System.out.println("NickName entered!");

    }


}
